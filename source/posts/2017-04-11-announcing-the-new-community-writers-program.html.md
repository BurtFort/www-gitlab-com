---
title: "Announcing the new Community Writers program"
author: Marcia Ramos
author_gitlab: marcia
author_twitter: XMDRamos
categories: GitLab news
image_title: '/images/community-writers/header-background-img.jpg'
description: "The Community Writers Program has changed. Understand why and what's new!"
ee_cta: false
---

The GitLab Community Writers Program has changed. Now our community contributions will be published
as [Technical Articles](https://docs.gitlab.com/ee/development/writing_documentation.html#technical-articles) in our [Documentation Portal](https://docs.gitlab.com). Continue reading to understand what else we've prepared for you.

<!-- more -->

----

## What is the Community Writers program?

You write, we publish, you earn up to USD 200 per article! 😃

Please check the [program webpage](/community-writers/) for a full overview, and the [terms and conditions](/community-writers/terms-and-conditions/) for all the details.

## What has changed?

The most significant changes are:

- **Publications**: Technical content now lives in the [documentation portal](https://docs.gitlab.com) only, including [technical articles](https://docs.gitlab.com/ee/development/writing_documentation.html#technical-articles) written by community writers.
- **[Requirements](/community-writers/terms-and-conditions/#requirements)**:
  - We are looking for authors with previous experience on writing technical content.
  - Make it work first, write afterwards: we expect you to make something work _before_ applying for the program.
  - Accountability: we expect you to have background experience on the subject you're willing to write about.
- **[Reviews](/community-writers/terms-and-conditions/#5-review)**:
  - Less reviews and less detailed reviews: we need your article to be submitted ready to publish, we don't have the manpower to help you to improve first.
  - Reviews will be more direct and concise, and will take a lot less time.
- **Evaluation**:
  - There will be two evaluations: [before you start writing](/community-writers/terms-and-conditions/#2-pre-assessment), and [when you send your draft](/community-writers/terms-and-conditions/#4-evaluation).
- **[Compensation criteria](/community-writers/terms-and-conditions/#compensation-criteria)**:
  - We evaluate your draft first, and offer the compensation later.
  - The compensation will not be based on the length of the article anymore, but on its quality and complexity.
- **[Draft](/community-writers/terms-and-conditions/#3-draft)**: your draft will be sent over email. Merge requests are to be opened once you have the approval from the editor and accept the compensation offer.
- **Issue tracker**: the [issue tracker](https://gitlab.com/gitlab-com/community-writers/issues) now contains only issues dedicated to the program.
- **Structure**: the [markdown](https://docs.gitlab.com/ee/user/markdown.html) and the [style guidelines](https://docs.gitlab.com/ee/development/doc_styleguide.html) now follow GitLab's documentation's.

## What has not changed?

- **Compensation**:
  - Once your article gets published, you get paid.
  - The compensation range remains the same: up to USD 200 per article.
- **[Writing method](/handbook/marketing/developer-relations/technical-writing/#writing-method)**: same requirements for QA: audience, outline, brainstorm, plan, research, draft, test, improve, and review.
- **Approach**: friendly, easy-following, informal, and engaging approach and writing style.
- **[Objective](/community-writers/terms-and-conditions/#purposes-of-the-program)**: the objective remains the same, producing high-quality resources to facilitate the use of GitLab.

## Why we changed the program

**Technical content in the right place**:
{:.gitlab-orange}

- Our blog is an extremely useful resource to communicate changes, make announcements, share stories, share our culture, and to promote new features and capabilities. Technical content was published less often on the blog, and wasn't as discoverable as it will be in the docs.
- Technical content has a permanent character when included in documentation, and ephemerous when in the blog.

**Maintenance and update**:
{:.gitlab-orange}

- Technical content present in our documentation can be more often maintained and kept up-to-date, once they live in the same repository as our code.

**Extensive reviews**:
{:.gitlab-orange}

- Reviewing community posts was taking a lot of time from our editors and from the authors, and therefore becoming slower and counterproductive. We decided to improve the evaluation process and its criteria, to review fast and publish faster. If the draft meets the standards, it will be approved to move forward. If not, we won't accept the content, saving everyone's time and effort.

**Compensation criteria**:
{:.gitlab-orange}

- With the new compensation criteria, we will be able to make better offers, based on the quality of the content, the creativity, the readability, and the complexity of the article. A short article can be much better than a long one, hence, base the payment range on the content length was far from ideal.

## Current proposals

We moved the most relevant issues from the [blog-post](https://gitlab.com/gitlab-com/blog-posts/issues) issue tracker to the new [community writers](https://gitlab.com/gitlab-com/community-writers/issues) tracker. We still might move others, and close some of them. If you have made a proposal there, or was waiting for evaluation of your proposal, we will update your issue soon. If you're happy the new terms, we'll move forward.

Community writers who had already submitted their content through merge requests to the blog will be oriented to move their content to the documentation. Compensation offers will remain intact for these cases.

New content and new proposals submitted to us by prospect community writers are subjected to the [new rules](/community-writers/terms-and-conditions/).

We apologize for the inconvenience caused by delays on reviews and feedback for your current proposals and articles. We wanted to improve the program to provide a better resource of technical content to our users and customers, and to offer you a better policy, as a community writer, before moving forward.
