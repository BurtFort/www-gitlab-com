---
layout: markdown_page
title: "Platform Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Platform Team
{: #platform}

The Platform Team is focused on all the other areas of GitLab that
the CI and Discussion Teams do not cover.

This team maps to [Create and Auth](/handbook/product/categories/#dev).
